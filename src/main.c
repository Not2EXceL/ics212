/*
 * Copyright (C) 2014-2015 Not2EXceL - Richmond Steele
 * **Totally didn't steal this from godshawk**
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdio.h>
#include <string.h>

typedef enum
{
    false, true
}
bool;

void getAddress(char address[], int size)
{
    int count = 0;
    int i = 0;
    char cont = '\0';
    char temp[size] = "";
    bool userQuit = false;
    //null out address to make checking for 79char limit easier
    for (i = 0; i < size; ++i)
    {
        address[i] = '\0';
    }
    do
    {
        printf("Enter address info: ");
        fgets(temp, size - count, stdin);
        fflush(stdin);
        for (i = 0; i < size; ++i)
        {
            if (temp[i] == '\0')
            {
                break;
            }
        }
        temp[++i] = '\n';
        strncpy(address + count, temp, i);
        count += (i - 1);

        printf("TEMP | %s\n", temp);
        printf("COUNT | %d\n", count);
        printf("ADDR | %s\n", address);
        if(i == size || count >= size) {
            break;
        }
        do
        {
            printf("Continue address input? (Y/y or N/n) ");
            scanf("%c\n", &cont);
            printf("\ncont | %c", cont);
            if (cont == 'Y' || cont == 'y')
            {
                userQuit = false;
                break;
            } else if (cont == 'N' || cont == 'n')
            {
                userQuit = true;
                break;
            }
        } while (true);
        if(count >= size || userQuit) {
            break;
        }
    } while (true);
    printf("End of addr func\n");
}


int main()
{
    char address[10] = "";
    getAddress(address, 10);
    printf("ADDRESS | %s\n", address);
    return 0;
}
