#include "func.h"

int main(int argc, char* argv[])
{
    int cost = 0;
    double input = -1;
    char line[100];

    while(1)
    {
        printf("Enter a length of road to paint: ");
        if(fgets(line, sizeof(line), stdin) != 0 &&
           sscanf(line, "%lf", &input) == 1)
        {
            if((int) input > 0)
            {
                break;
            } else
            {
                printf("Failed to input a value greater than zero. Please try again.\n");
            }
        } else
        {
            printf("Failed to input a double value. Please try again.\n");
        }
    }
    cost = costofpainting(input);
    printf("Cost to paint the road: %d\n", cost);
}
