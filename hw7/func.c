#include "func.h"

int costofpainting(double length) {
    double third = 0;
    if(length <= 2) {
        return 200;
    } else {
        third = length / 3.0;
        return 100 + (costofpainting(third) * 3);
    }
}
