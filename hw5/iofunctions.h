/*****************************************************************
//
// NAME: Richmond Steele
//
// HOMEWORK: 5
//
// CLASS: ICS 212
//
// INSTRUCTOR: Ravi Narayan
//
// DATE: February 20, 2016
//
// FILE: iofunctions.h
//
// DESCRIPTION: This file contains the prototypes for readfile and writefile
//
//****************************************************************/

#ifndef ICS212_IOFUNCIONS_H
#define ICS212_IOFUNCIONS_H

#include <stdio.h>
#include <string.h>
#include "account.h"

void readfile(struct account accarray[], int* numcust, char filename[]);

void writefile(struct account accarray[], int numcust, char filename[]);

#endif //ICS212_IOFUNCIONS_H
