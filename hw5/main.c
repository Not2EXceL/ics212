/*****************************************************************
//
// NAME: Richmond Steele
//
// HOMEWORK: 5
//
// CLASS: ICS 212
//
// INSTRUCTOR: Ravi Narayan
//
// DATE: February 20, 2016
//
// FILE: main.c
//
// DESCRIPTION: This file contains the main function
//
//****************************************************************/

#include <stdlib.h>

#include "account.h"
#include "iofunctions.h"

void printAccounts(struct account[], int);

/*****************************************************************
//
// Function name: main
//
// DESCRIPTION: A main function that runs test functions and provides them with test data
//
// Parameters: 2 (int, char**) : number of arguments, arguments array pointer
//
// Return values: 0 : exit value, -1 if argument error
//
//****************************************************************/
int main(int argc, char** argv)
{
    char* filename = "accounts.txt";
    char* accNames[] = { "One", "Two", "Three", "Four", "Five" };
    struct account bankone[5];
    int numcustomers = 5;
    int count = 0;
    int i = 0;

    for(count = 0; count < numcustomers; ++count)
    {
        printf("Sample Input account data:\n");
        strncpy(bankone[count].name, accNames[count], 24);
        bankone[count].accountno = count + 1;
        bankone[count].balance = rand() % 100;
        printf("  Name: %s\n  Accountno: %d\n  Balance: %f\n",
               bankone[count].name,
               bankone[count].accountno,
               bankone[count].balance);
    }

    printf("Printing initialized data...\n");
    printAccounts(bankone, numcustomers);

    writefile(bankone, numcustomers, filename);

    //scrambling bankone accountno and balance data so that they should not match the initialized data
    for(count = 0; count < numcustomers; ++count)
    {
        bankone[count].accountno = rand() % 100 + count;
        bankone[count].balance = rand() % 100;
    }

    printf("\nReading account data...\n");

    readfile(bankone, &numcustomers, filename);

    printf("\nPrinting read data...\n");
    printAccounts(bankone, numcustomers);
}

/*****************************************************************
//
// Function name: printAccounts
//
// DESCRIPTION: prints the account array in human readable format
//
// Parameters: 2 (struct account[], int) : the account array, number of accounts
//
// Return values: 0 :
//
//****************************************************************/
void printAccounts(struct account accarray[], int count)
{
    int i = 0;
    for(i = 0; i < count; ++i)
    {
        printf("  Name: %5s AccountNo: %d Balance: %.2f\n",
               accarray[i].name,
               accarray[i].accountno,
               accarray[i].balance);
    }
}
