/*****************************************************************
//
// NAME: Richmond Steele
//
// HOMEWORK: 5
//
// CLASS: ICS 212
//
// INSTRUCTOR: Ravi Narayan
//
// DATE: February 20, 2016
//
// FILE: account.h
//
// DESCRIPTION: This file contains the account struct
//
//****************************************************************/

#ifndef ICS212_ACOUNT_H
#define ICS212_ACOUNT_H

struct account
{
    char         name[25];
    int          accountno;
    float        balance;

};

#endif //ICS212_ACOUNT_H
