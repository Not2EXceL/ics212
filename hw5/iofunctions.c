/*****************************************************************
//
// NAME: Richmond Steele
//
// HOMEWORK: 5
//
// CLASS: ICS 212
//
// INSTRUCTOR: Ravi Narayan
//
// DATE: February 20, 2016
//
// FILE: iofunctions.c
//
// DESCRIPTION: This file contains the definitions for readfile and writefile
//
//****************************************************************/
#include "iofunctions.h"
#include "account.h"
#include <float.h>

/*****************************************************************
//
// Function name: readfile
//
// DESCRIPTION: Reads from the provided filename, the account data into the passed array
//
// Parameters: 3 (struct account[], int, cha[]) : account array, number of accounts, filename
//
// Return values: 0 :
//
//****************************************************************/
void readfile(struct account accarray[], int* numcust, char filename[])
{
    FILE* file;
    int count = 0;
    char tempName[25] = { 0 };
    int tempAccountNo = 0;
    float tempBalance = 0;
    char buffer[100];

    file = fopen(filename, "r");

    while(count < *numcust && fgets(buffer, sizeof(buffer), file)) {
        sscanf(buffer, "Name: %s AccountNo: %d Balance: %f\n",
               tempName, &tempAccountNo, &tempBalance);
        strncpy(accarray[count].name, tempName, 24);
        accarray[count].accountno = tempAccountNo;
        accarray[count].balance = tempBalance;
        ++count;
    }
    fclose(file);
}


/*****************************************************************
//
// Function name: writefile
//
// DESCRIPTION: Writes to the provided filename, the account data from the passed array
//
// Parameters: 3 (struct account[], int, cha[]) : account array, number of accounts, filename
//
// Return values: 0 :
//
//****************************************************************/
void writefile(struct account accarray[], int numcust, char filename[])
{
    FILE* file;
    int count = 0;

    file = fopen(filename, "w+");

    for(count = 0; count < numcust; ++count) {
        fprintf(file, "Name: %s AccountNo: %d Balance: %f\n",
                accarray[count].name,
                accarray[count].accountno,
                accarray[count].balance);
    }
    fclose(file);
}
