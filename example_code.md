/*****************************************************************
//
// NAME: Ravi Narayan
//
// HOMEWORK: 1
//
// CLASS: ICS 212
//
// INSTRUCTOR: Ravi Narayan
//
// DATE: May 30, 2020
//
// FILE: hw1main.c
//
// DESCRIPTION: This file contains the driver and the user-interface functions
// for Homework 1 – the temperature conversion program
//
//****************************************************************/ 

#include <stdio.h>

void convertFtoC(int, float );

/*****************************************************************
//
// Function name: getinput
//
// DESCRIPTION: A userinterface function ……
// This function obtains the values of the … from the user
//
// Parameters: count (int) : contains the number of arguments which will be
// processed
//
// Return values: 0 : success
// -1 : the value was not found
//
//****************************************************************/

int getinput(int count)
{
    int x, i
    
    for (i = 0; i < count; i++)
        {
            if (…)
            {
                printf(…);
            }
        }
    }
} 
