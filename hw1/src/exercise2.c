#include <stdio.h>

void dummy(int, char, char *, float);

int main(int argc, char* argv[])
{
    int num1, num2 = 13;
    char c = 'H';
    float score1 = 12.5;
    char ca[3] = "Hi";


    printf("[main] argc: %d, &argc: %p\n", argc, (void*)&argc);

    printf("[main] argv0: %c, &argv0: %p\n", *argv, (void*)argv);

    printf("[main] num1: %d, &num1: %p\n", num1, (void*)&num1);
    printf("[main] num2: %d, &num2: %p\n", num2, (void*)&num2);

    printf("[main] c: %c, &c: %p\n", c, (void*)&c);
    printf("[main] score1: %f, &score1: %p\n", score1, (void*)&score1);

    printf("[main] ca0: %c, &ca0: %p\n", ca[0], (void*)&ca);
    printf("[main] ca1: %c, &ca1: %p\n", ca[1], (void*)&ca[1]);
    printf("[main] ca2: %c, &ca2: %p\n", ca[2], (void*)&ca[2]);

    dummy(num2, c, ca, score1);

    return 0;
}

void dummy(int x, char y, char * z, float w)
{
    printf("[dummy] x: %d, &x: %p\n", x, (void*)&x);
    printf("[dummy] y: %c, &y: %p\n", y, (void*)&y);
    printf("[dummy] z: %c, &z: %p\n", *z, (void*)z);
    printf("[dummy] w: %f, &w: %p\n", w, (void*)&w);

    x++;
    printf("[dummy] x: %d, &x: %p\n", x, (void*)&x);

    y++;
    printf("[dummy] y: %c, &y: %p\n", y, (void*)&y);

    w = w + 2.3f;
    printf("[dummy] w: %f, &w: %p\n", w, (void*)&w);
}
