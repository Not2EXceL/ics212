/*
*   NAME: Richmond Steele
*
*   HOMEWORK: 1
*
*   CLASS: ICS 212
*
*   INSTRUCTOR: Ravi Narayan
*
*   DATE: January 24, 2016
*
*   FILE: hw1main.c
*
*   DESCRIPTION: This file contains the driver and the user-interface functions
*   for Homework 1 – the temperature conversion program
*
*
*   The class coding style guidelines (allman indent style) are not
*      nearly used as heavily in production anymore.
*   Even the linux kernel enforces K&R indent style with the exception in the case of a function definition.
*   https://www.kernel.org/doc/Documentation/CodingStyle
*/

#include <stdio.h>

int requestUserTemperature();

float convertFtoC(int);

int roundMaxTemp(int);

int validateMultipleFive(int);

void printTable(int);

/*
*
*    Function name: main
*
*    DESCRIPTION: Runs this temperature conversion program
*
*    Parameters: 0 ():
*
*    Return values: application exit code
*
*/
int main()
{
    char ca[3] = "Hi";
    printf("%p\n", (void*)ca[0]);
    printf("%p\n", (void*)ca[1]);
    printf("%p\n", (void*)ca[2]);
    int value = requestUserTemperature();
    int rounded = roundMaxTemp(value);
    printTable(rounded);
    return 0;
}

/*
 *
 *    Function name: requestUserTemperature
 *
 *    DESCRIPTION: Requests user input, checks that is an int and that is greater than 0
 *          Continuously requests till valid input is submitted
 *
 *    Parameters: 0 ():
 *
 *    Return values: Valid integer user input
 *
 */
int requestUserTemperature()
{
    float input = -1;
    char line[4096];

    printf("This program takes an input temperature in F,\n" \
                       "and displays a table of bother F and C temperatures " \
                       "by increments of 5.\n\n");

    while(1)
    {
        printf("Enter a Temperature in F: ");
        if(fgets(line, sizeof(line), stdin) != 0 &&
           sscanf(line, "%f", &input) == 1)
        {
            if((int) input > 0)
            {
                break;
            } else
            {
                printf("Failed to input a value greater than zero. Please try again.\n");
            }
        } else
        {
            printf("Failed to input a numerical value. Please try again.\n");
        }
    }
    return (int) input;
}

/*
 *
 *    Function name: roundMaxTemp
 *
 *    DESCRIPTION: Rounds the passed value to the nearest multiple of 5 greater than or
 *          equal to itself
 *
 *    Parameters: 1 (int): The value to round
 *
 *    Return values: rounded temperature value
 *
 */
int roundMaxTemp(int maxTemp)
{
    int diff = maxTemp % 5;
    return maxTemp + ((diff == 0) ? 0 : (5 - diff));
}

/*
 *
 *    Function name: convertFtoC
 *
 *    DESCRIPTION: converts a temperature from F to C
 *
 *    Parameters: 1 (int): F temperature to convert to C
 *
 *    Return values: C temperature equivalent of passed F temperature
 *
 */
float convertFtoC(int temp)
{
    return (temp - 32) * (5 / (float) 9);
}

/*
 *
 *    Function name: validateMultipleFive
 *
 *    DESCRIPTION: Validates that the passed value is a multiple of 5
 *
 *    Parameters: 1 (int): The value to check
 *
 *    Return values:
 *          0: multiple of 5
 *          -1: not multiple of 5
 *
 */
int validateMultipleFive(int value)
{
    /*
     * According to linux kernel guidelines, this function should look like this.
     int validateMultipleFive(int value)
     {
         if(value % 5 == 0) {
            return 0;
         } else {
            return -1;
         }
     }
     */
    if(value % 5 == 0)
    {
        return 0;
    } else
    {
        return -1;
    }
}

/*
*
*    Function name: printTable
*
*    DESCRIPTION: Prints the table of temperatures in F and C in increments of 5
*
*    Parameters: 1 (int): The max temperature that the table should print to
*
*    Return values: N/A
*
*/
void printTable(int maxTemp)
{
    int count = 0;

    if(validateMultipleFive(maxTemp) == -1)
    {
        maxTemp = roundMaxTemp(maxTemp);
    }
    printf("Temperature in F");
    printf("    ");
    printf("Temperature in C\n");

    /*
     * The for loop version of the while loop required for this function, much simpler and less messy
     *
    for (int i = 0; i <= maxTemp; i += 5) {
        printf("%16d    %16.2f\n", i, convertFtoC(i));
    }
    */

    do
    {
        printf("%16d    %16.2f\n", count, convertFtoC(count));
    } while((count += 5) <= maxTemp);
}
