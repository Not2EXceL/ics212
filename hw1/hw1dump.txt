bash-3.2$ ls
hw
bash-3.2$ cd hw/hw1
bash-3.2$ ls
exercise2    exercise2.c  hw1main      hw1main.c
bash-3.2$ cc -c hw1main.c
bash-3.2$ ls
exercise2    exercise2.c  hw1main      hw1main.c    hw1main.o
bash-3.2$ cc -c hw1main.c
bash-3.2$ cc -strict -c hw1main.c
bash-3.2$ cc -strict hw1main.c -o hw1main
bash-3.2$ ls
exercise2    exercise2.c  hw1main      hw1main.c    hw1main.o
bash-3.2$ ./hw1main
bash: ./hw1main: Invalid argument
bash-3.2$ cc hw1main.c -o hw1main
bash-3.2$ ./hw1main
This program takes an input temperature in F,
and displays a table of bother F and C temperatures by increments of 5.

Enter a Temperature in F: 14
Temperature in F    Temperature in C
               0              -17.78
               5              -15.00
              10              -12.22
              15               -9.44
bash-3.2$ ./hw1main
This program takes an input temperature in F,
and displays a table of bother F and C temperatures by increments of 5.

Enter a Temperature in F: 29
Temperature in F    Temperature in C
               0              -17.78
               5              -15.00
              10              -12.22
              15               -9.44
              20               -6.67
              25               -3.89
              30               -1.11
bash-3.2$ ./hw1main
This program takes an input temperature in F,
and displays a table of bother F and C temperatures by increments of 5.

Enter a Temperature in F: 87
Temperature in F    Temperature in C
               0              -17.78
               5              -15.00
              10              -12.22
              15               -9.44
              20               -6.67
              25               -3.89
              30               -1.11
              35                1.67
              40                4.44
              45                7.22
              50               10.00
              55               12.78
              60               15.56
              65               18.33
              70               21.11
              75               23.89
              80               26.67
              85               29.44
              90               32.22
bash-3.2$
