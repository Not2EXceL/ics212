/*****************************************************************
//
// NAME: Richmond Steele
//
// HOMEWORK: 3
//
// CLASS: ICS 212
//
// INSTRUCTOR: Ravi Narayan
//
// DATE: February 06, 2016
//
// FILE: database.h
//
// DESCRIPTION: The database header file that just contains the prototypes for the database stub functions
//
//****************************************************************/

#ifndef DATABASE_H
#define DATABASE_H

#include "record.h"

int addRecord(struct record**, int, char [], char [], int);

int printRecord(struct record*, int);

int modifyRecord(struct record*, int, char []);

void printAllRecords(struct record*);

int deleteRecord(struct record**, int);

#endif
