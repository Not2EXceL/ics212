/*****************************************************************
//
// NAME: Richmond Steele
//
// HOMEWORK: 3
//
// CLASS: ICS 212
//
// INSTRUCTOR: Ravi Narayan
//
// DATE: February 06, 2016
//
// FILE: database.c
//
// DESCRIPTION: Contains the database stub functions
//
//****************************************************************/

#include "database.h"
#include "hw3main.h"

/*****************************************************************
//
// Function name: addRecord
//
// DESCRIPTION: stubbed function
//
// Parameters: 5 (struct record*, int, char[], char[], int) :
//              pointer to record, account number as key, name to store, address to store, year of birth
//
// Return values: 1 : int, 0 stubbed function
//
//****************************************************************/
int addRecord(struct record** records, int accountno, char name[], char address[], int yearofbirth)
{
    DEBUG("[addRecord] records: %p, accountno: %d, name: %s, address: { %s }, yearofbirth: %d",
          records, accountno, name, address, yearofbirth);
    printf("Adding record number: %d");
    return 0;
}

/*****************************************************************
//
// Function name: printRecord
//
// DESCRIPTION: stubbed function
//
// Parameters: 2 (struct record*, int) : pointer to record, account number as key
//
// Return values: 1 : int, 0 stubbed function
//
//****************************************************************/
int printRecord(struct record* _record, int accountno)
{
    DEBUG("[printRecord] record: %p, accountno: %d", _record, accountno);
    printf("Printing info on record number: %d", accountno);
    return 0;
}

/*****************************************************************
//
// Function name: modifyRecord
//
// DESCRIPTION: stubbed function
//
// Parameters: 3 (struct record*, int, char[]) : pointer to record, account number as key, address to store
//
// Return values: 1 : int, 0 stubbed function
//
//****************************************************************/
int modifyRecord(struct record* _record, int accountno, char address[])
{
    DEBUG("[modifyRecord] record: %p, accountno: %d, address: { %s }", _record, accountno, address);
    printf("Modifying record number: %d to have address: { %s }", accountno, address);
    return 0;
}

/*****************************************************************
//
// Function name: printAllRecords
//
// DESCRIPTION: stubbed function
//
// Parameters: 1 (struct record*) : pointer to record
//
// Return values: 0 :
//
//****************************************************************/
void printAllRecords(struct record* start)
{
    DEBUG("[printAllRecords] record: %p", start);
    printf("Printing all records in database.");
}

/*****************************************************************
//
// Function name: deleteRecord
//
// DESCRIPTION: stubbed function
//
// Parameters: 2 (struct record**, int) : pointer to first record, accountno as key
//
// Return values: 1 : int, 0 due to stub
//
//****************************************************************/
int deleteRecord(struct record** records, int accountno)
{
    DEBUG("[deleteRecord] records: %p, accountno: %d", records, accountno);
    printf("Deleting record number: %d", accountno);
    return 0;
}
