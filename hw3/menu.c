/*****************************************************************
//
// NAME: Richmond Steele
//
// HOMEWORK: 3
//
// CLASS: ICS 212
//
// INSTRUCTOR: Ravi Narayan
//
// DATE: February 06, 2016
//
// FILE: menu.c
//
// DESCRIPTION: Contains the menu ui
//
//****************************************************************/

/*
    1 Add a new record in the database
    2 Modify a record in the database using the accountno as the key
    3 Print information about a record using the accountno as the key
    4 Print all information in the database
    5 Delete an existing record from the database using the accountno as a key
    6 Quit the program
*/

#include <string.h>
#include "menu.h"
#include "database.h"

/*****************************************************************
//
// Function name: menuUI
//
// DESCRIPTION: driver for the TUI
//
// Parameters: 1 (struct record**) : pointer to first record
//
// Return values: 0 :
//
//****************************************************************/
void menuUI(struct record** records)
{
    int addressLength = 80;
    int nameLength = 25;
    bool hasQuit = false;
    int choice = 0;
    int accountNumber = 0;
    char name[nameLength];
    char address[addressLength];
    int i;
    int yearofbirth = 0;
    for(i = 0; i < addressLength || i < nameLength; ++i)
    {
        if(i < addressLength)
        {
            address[i] = '\0';
        }
        if(i < nameLength)
        {
            name[i] = '\0';
        }
    }

    DEBUG("[menuUI]%p", records);
    do
    {
        printBreaker();
        printMenu();
        scanf(" %d", &choice);
        getchar();
        switch(choice)
        {
            case 1:
                accountNumber = getAccountNumber();
                getName(name, nameLength);
                getAddress(address, addressLength);
                yearofbirth = getYearOfBirth();
                addRecord(records, accountNumber, name, address, yearofbirth);
                break;
            case 2:
                accountNumber = getAccountNumber();
                getAddress(address, addressLength);
                modifyRecord(*records, accountNumber, address);
                break;
            case 3:
                accountNumber = getAccountNumber();
                printRecord(*records, accountNumber);
                break;
            case 4:
                printAllRecords(*records);
                break;
            case 5:
                accountNumber = getAccountNumber();
                deleteRecord(records, accountNumber);
                break;
            case 6:
                printf("Exiting...");
                hasQuit = true;
                break;
            default:
                continue;
        }
    } while(!hasQuit);
}

/*****************************************************************
//
// Function name: printMenu
//
// DESCRIPTION: prints the menu
//
// Parameters: 0 () :
//
// Return values: 0 :
//
//****************************************************************/
void printMenu()
{
    DEBUG("[printMenu]");
    printf("Database Menu:\n");
    printf("  1) Add record.\n");
    printf("  2) Modify record.\n");
    printf("  3) Print record info.\n");
    printf("  4) Print entire database.\n");
    printf("  5) Delete record.\n");
    printf("  6) Quit.\n");
    printf("Enter choice (number): ");
}

/*****************************************************************
//
// Function name: printBreaker
//
// DESCRIPTION: prints the 3 line breaker, 2 newlines wrapping 30 '=' chars
//
// Parameters: 0 () :
//
// Return values: 0 :
//
//****************************************************************/
void printBreaker()
{
    DEBUG("[printBreaker]");
    //just print 3 line breaker, curses would be the better choice for a clear
    printf("\n");
    printf("==============================\n");
    printf("\n");
}

/*****************************************************************
//
// Function name: getAccountNumber
//
// DESCRIPTION: gets the user input account number
//
// Parameters: 0 () :
//
// Return values: 1 : account number
//
//****************************************************************/
int getAccountNumber()
{
    int accountno;
    DEBUG("[getAccountNumber]");
    printf("Enter an account number: ");
    scanf(" %d", &accountno);
    getchar();
    return accountno;
}

/*****************************************************************
//
// Function name: getYearOfBirth
//
// DESCRIPTION: gets the user input birth year
//
// Parameters: 0 () :
//
// Return values: 1 : birth year
//
//****************************************************************/
int getYearOfBirth()
{
    int yearofbirth;
    DEBUG("[getYearOfBirth]");
    printf("Enter the account birth year: ");
    scanf(" %d", &yearofbirth);
    getchar();
    fflush(stdout);
    return yearofbirth;
}

/*****************************************************************
//
// Function name: getName
//
// DESCRIPTION: gets the user input account name
//
// Parameters: 2 (char[], int) : the name char array, size of name char array
//
// Return values: 0 :
//
//****************************************************************/
void getName(char name[], int size)
{
    DEBUG("[getName] name: %s, size: %d", name, size);
    printf("Enter name: ");
    fgets(name, size, stdin);
}

/*****************************************************************
//
// Function name: getAddress
//
// DESCRIPTION: gets the user input account address
//
// Parameters: 2 (char[], int) : the address char array, size of address char array
//                                  this function accepts up to 79 chars for the address over multiple lines
//                                  due to array in record being 80 char max, need null char and halts unnecessary input
//
// Return values: 0 :
//
//****************************************************************/
void getAddress(char address[], int size)
{
    int count = 0;
    int i = 0;
    char cont = '\0';
    char temp[size];
    bool userQuit = false;
    DEBUG("[getAddress] address: %s, size: %d", address, size);
    //null out address to make checking for 79char limit easier
    for(i = 0; i < size; ++i)
    {
        address[i] = '\0';
        temp[i] = '\0';
    }
    do
    {
        printf("Enter address info: ");
        fgets(temp, size - count, stdin);
        for(i = 0; i < size; ++i)
        {
            if(temp[i] == '\0')
            {
                break;
            }
        }
        temp[++i] = '\n';
        strncpy(address + count, temp, i);
        count += (i - 1);

        if(i == size || count >= size)
        {
            break;
        }
        do
        {
            printf("Continue address input? (Y/y or N/n) ");
            scanf(" %c", &cont);
            getchar();
            if(cont == 'Y' || cont == 'y')
            {
                userQuit = false;
                break;
            } else if(cont == 'N' || cont == 'n')
            {
                userQuit = true;
            } else
            {
                continue;
            }
        } while(!userQuit);
    } while(!userQuit);
}
