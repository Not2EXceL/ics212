/*****************************************************************
//
// NAME: Richmond Steele
//
// HOMEWORK: 3
//
// CLASS: ICS 212
//
// INSTRUCTOR: Ravi Narayan
//
// DATE: February 06, 2016
//
// FILE: menu.h
//
// DESCRIPTION: Contains the menu ui prototypes
//
//****************************************************************/

#ifndef MENU_H
#define MENU_H

#include "hw3main.h"
#include "record.h"
#include <stdlib.h>

void menuUI(struct record**);

void printMenu();

void printBreaker();

int getAccountNumber();

int getYearOfBirth();

void getName(char[], int);

void getAddress(char[], int);

#endif
