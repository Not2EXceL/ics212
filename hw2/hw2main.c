/*****************************************************************
//
// NAME: Richmond Steele
//
// HOMEWORK: 2b
//
// CLASS: ICS 212
//
// INSTRUCTOR: Ravi Narayan
//
// DATE: January 31, 2016
//
// FILE: hw2main.c
//
// DESCRIPTION: This file contains the main and test functions for functions defined in hw2func.c
//
//****************************************************************/

#include "hw2func.h"

void testBubbleSort(int array[], int length);

void testHalfString(char input[], char output[]);

void testSimpleHalfString(char input[], char output[]);

/*****************************************************************
//
// Function name: main
//
// DESCRIPTION: A main function that runs test functions and provides them with test data
//
// Parameters: 0 () :
//
// Return values: 0 : exit value, no need to return anything but 0 atm
//
//****************************************************************/
int main()
{
    int intArray0[] = { 4, 3, 1, 2, 6, 5 };
    int length0 = 6;
    int intArray1[] = { 65, 1, 978, 36, 41 };
    int length1 = 5;
    int intArray2[] = { 9, 8, 7, 6, 5, 4, 3, 2, 1 };
    int length2 = 9;
    char input0[] = "abc_def_";
    char output0[] = "________";
    char input1[] = "halfthis_notthis";
    char output1[] = "________________";
    char input2[] = "123456";
    char output2[] = "______";

    testBubbleSort(intArray0, length0);
    testBubbleSort(intArray1, length1);
    testBubbleSort(intArray2, length2);

    testHalfString(input0, output0);
    testHalfString(input1, output1);
    testHalfString(input2, output2);

    testSimpleHalfString(input0, output0);
    testSimpleHalfString(input1, output1);
    testSimpleHalfString(input2, output2);
}

/*****************************************************************
//
// Function name: testBubbleSort
//
// DESCRIPTION: A test function to avoid duplication the test output and call to bubblesort
//
// Parameters: 2 (int[], int) : Same as bubblesort as this dispatches and prints relevant data
//
// Return values: 0
//
//****************************************************************/
void testBubbleSort(int array[], int length)
{
    int i;
    printf("Unsorted Array: { ");
    for(i = 0; i < length; ++i)
    {
        printf("%d ", array[i]);
    }
    printf("} \n");
    bubblesort(array, length);
    printf("Sorted Array:   { ");
    for(i = 0; i < length; ++i)
    {
        printf("%d ", array[i]);
    }
    printf("} \n\n");
}

/*****************************************************************
//
// Function name: testHalfString
//
// DESCRIPTION: A test function to avoid duplication the test output and call to halfstring
//
// Parameters: 2 (char[], char[]) : Same as halfstring as this dispatches and prints relevant data
//
// Return values: 0
//
//****************************************************************/
void testHalfString(char input[], char output[])
{
    int i = 0;
    int j = 0;
    halfstring(input, output);
    printf("Full String: ");
    while(input[i])
    {
        printf("%c", input[i]);
        ++i;
    }
    printf("\n");
    printf("HalfString: ");
    while(output[j])
    {
        printf("%c", output[j]);
        ++j;
    }
    printf("\n\n");
}

/*****************************************************************
//
// Function name: testSimpleHalfString
//
// DESCRIPTION: A test function to avoid duplication the test output and call to simplehalfstring
//
// Parameters: 2 (char[], char[]) : Same as simplehalfstring as this dispatches and prints relevant data
//
// Return values: 0
//
//****************************************************************/
void testSimpleHalfString(char input[], char output[])
{
    int i = 0;
    int j = 0;
    simplehalfstring(input, output);
    printf("Full String: ");
    while(input[i])
    {
        printf("%c", input[i]);
        ++i;
    }
    printf("\n");
    printf("HalfString: ");
    while(output[j])
    {
        printf("%c", output[j]);
        ++j;
    }
    printf("\n\n");
}
