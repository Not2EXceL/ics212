/*****************************************************************
//
// NAME: Richmond Steele
//
// HOMEWORK: 2b
//
// CLASS: ICS 212
//
// INSTRUCTOR: Ravi Narayan
//
// DATE: January 31, 2016
//
// FILE: hw2func.h
//
// DESCRIPTION: This file contains the function declarations for hw2func.c
//
//****************************************************************/

#ifndef HW2_FUNC_H
#define HW2_FUNC_H

#include <stdio.h>

int bubblesort(int [], int);

void halfstring(char [], char []);

void simplehalfstring(char [], char []);

#endif
