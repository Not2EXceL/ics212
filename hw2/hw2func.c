/*****************************************************************
//
// NAME: Richmond Steele
//
// HOMEWORK: 2b
//
// CLASS: ICS 212
//
// INSTRUCTOR: Ravi Narayan
//
// DATE: January 31, 2016
//
// FILE: hw2main.c
//
// DESCRIPTION: This file contains the bubblesort (for int arrays), and both functions to get the first half of a string, with and without std lib calls
//
//****************************************************************/

#include <string.h>
#include "hw2func.h"

/*****************************************************************
//
// Function name: bubblesort
//
// DESCRIPTION: A function that sorts a provided integer array of specified length via bubble sort method
//
// Parameters: 2 (int[], int) : input array, and length
//
// Return values: 1 : 0, not sure where a -1 for a failure will be fired, since c does not manage array bounds
//
//****************************************************************/
int bubblesort(int array[], int length)
{
    int i, k, temp;
    for(i = 0; i < length; ++i)
    {
        for(k = 0; k < length - 1; ++k)
        {
            if(array[k] > array[k + 1])
            {
                temp = array[k];
                array[k] = array[k + 1];
                array[k + 1] = temp;
            }
        }
    }
    return 0;
}

/*****************************************************************
//
// Function name: halfstring
//
// DESCRIPTION: A function that copies the first half of a string into the provided output array
//                 This function does not utilize std lib functions, but instead similar logic
//
// Parameters: 2 (char[], char[]) : input and output
//
// Return values: 0
//
//****************************************************************/
void halfstring(char input[], char output[])
{
    size_t inputLength = 0;
    size_t halfInput;
    int i;
    while(input[inputLength]) ++inputLength;
    halfInput = inputLength / 2;

    for(i = 0; i < inputLength; ++i)
    {
        if(i < halfInput)
        {
            output[i] = input[i];
        } else
        {
            output[i] = '\0';
        }
    }
}

/*****************************************************************
//
// Function name: simplehalfstring
//
// DESCRIPTION: A function that copies the first half of a string into the provided output array
//                 This function utilizes std lib functions
//
// Parameters: 2 (char[], char[]) : input and output
//
// Return values: 0
//
//****************************************************************/
void simplehalfstring(char input[], char output[])
{
    size_t halfInput;
    size_t inputLength = strlen(input);
    halfInput = inputLength / 2;
    strncpy(output, input, inputLength - halfInput);
}
