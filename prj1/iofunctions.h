/*****************************************************************
//
// NAME: Richmond Steele
//
// HOMEWORK: 5
//
// CLASS: ICS 212
//
// INSTRUCTOR: Ravi Narayan
//
// DATE: February 20, 2016
//
// FILE: iofunctions.h
//
// DESCRIPTION: This file contains the prototypes for readfile and writefile
//
//****************************************************************/

#ifndef IOFUNCIONS_H
#define IOFUNCIONS_H

#include <stdio.h>
#include <string.h>
#include <float.h>
#include "main.h"
#include "record.h"

int readfile(struct record**, char []);

void writefile(struct record*, char []);

#endif //IOFUNCIONS_H
