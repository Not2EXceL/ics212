/*****************************************************************
//
// NAME: Richmond Steele
//
// HOMEWORK: 3
//
// CLASS: ICS 212
//
// INSTRUCTOR: Ravi Narayan
//
// DATE: February 06, 2016
//
// FILE: database.c
//
// DESCRIPTION: Contains the database stub functions
//
//****************************************************************/

#include "database.h"
#include "main.h"

/*****************************************************************
//
// Function name: addRecord
//
// DESCRIPTION: stubbed function
//
// Parameters: 5 (struct record*, int, char[], char[], int) :
//              pointer to record, account number as key, name to store, address to store, year of birth
//
// Return values: 1 : int, 0 stubbed function
//
//****************************************************************/
int addRecord(struct record** records, int accountno, char name[], char address[], int yearofbirth)
{
    struct record* _record = NULL;
    struct record* newRecord = (struct record*) malloc(sizeof(struct record));

    DEBUG("[addRecord] records: %p, accountno: %d, name: %s, address: { %s }, yearofbirth: %d",
          records, accountno, name, address, yearofbirth);
    newRecord->accountno = accountno;
    strncpy(newRecord->name, name, 24);
    strncpy(newRecord->address, address, 79);
    newRecord->yearofbirth = yearofbirth;
    if(records[0] == NULL)
    {
        records[0] = newRecord;
    } else
    {
        _record = *records;
        while(_record->next != NULL)
        {
            _record = _record->next;
        }
        _record->next = newRecord;
    }
    return 1;
}

/*****************************************************************
//
// Function name: printRecord
//
// DESCRIPTION: stubbed function
//
// Parameters: 2 (struct record*, int) : pointer to record, account number as key
//
// Return values: 1 : int, 0 stubbed function
//
//****************************************************************/
int printRecord(struct record* _record, int accountno)
{
    DEBUG("[printRecord] record: %p, accountno: %d", _record, accountno);
    if(_record == NULL)
    {
        printf("There are no records to print.\n");
    } else
    {
        while(_record != NULL)
        {
            if(_record->accountno == accountno)
            {
                printf("[Account] Number: %d, Name: %s, YearOfBirth: %d\n",
                       _record->accountno, _record->name, _record->yearofbirth);
                printf("[Account] Address:\n%s\n", _record->address);
            }
            _record = _record->next;
        }
    }
    return 1;
}

/*****************************************************************
//
// Function name: modifyRecord
//
// DESCRIPTION: stubbed function
//
// Parameters: 3 (struct record*, int, char[]) : pointer to record, account number as key, address to store
//
// Return values: 1 : int, 0 stubbed function
//
//****************************************************************/
int modifyRecord(struct record* _record, int accountno, char address[])
{
    DEBUG("[modifyRecord] record: %p, accountno: %d, address: { %s }", _record, accountno, address);
    if(_record == NULL)
    {
        printf("There are no records to modify.\n");
    } else
    {
        while(_record != NULL)
        {
            if(_record->accountno == accountno)
            {
                printf("[Modifying] Number: %d\n", _record->accountno);
                strncpy(_record->address, address, 79);
            }
            _record = _record->next;
        }
    }
    return 0;
}

/*****************************************************************
//
// Function name: printAllRecords
//
// DESCRIPTION: stubbed function
//
// Parameters: 1 (struct record*) : pointer to record
//
// Return values: 0 :
//
//****************************************************************/
void printAllRecords(struct record* start)
{
    DEBUG("[printAllRecords] record: %p", start);
    if(start == NULL)
    {
        printf("There are no records to print.\n");
    } else
    {
        while(start != NULL)
        {
            printf("[Account] Number: %d, Name: %s, YearOfBirth: %d\n",
                   start->accountno, start->name, start->yearofbirth);
            printf("[Account] Address:\n%s\n", start->address);
            start = start->next;
        }
    }
}

/*****************************************************************
//
// Function name: deleteRecord
//
// DESCRIPTION: stubbed function
//
// Parameters: 2 (struct record**, int) : pointer to first record, accountno as key
//
// Return values: 1 : int, 0 due to stub
//
//****************************************************************/
int deleteRecord(struct record** records, int accountno)
{
    struct record* curRecord = NULL;
    struct record* prevRecord = NULL;
    DEBUG("[deleteRecord] records: %p, accountno: %d", records, accountno);
    if(records[0] == NULL)
    {
        printf("There are no records to delete.\n");
    } else
    {
        curRecord = *records;
        while(curRecord != NULL)
        {
            if(curRecord->accountno == accountno)
            {
                printf("[Deleting] Number: %d\n", curRecord->accountno);
                if(curRecord == *records)
                {
                    records[0] = curRecord->next;
                } else
                {
                    prevRecord->next = curRecord->next;
                }
            }
            prevRecord = curRecord;
            curRecord = curRecord->next;
        }
    }
    return 0;
}
