/*****************************************************************
//
// NAME: Richmond Steele
//
// HOMEWORK: 3
//
// CLASS: ICS 212
//
// INSTRUCTOR: Ravi Narayan
//
// DATE: February 06, 2016
//
// FILE: hw3main.h
//
// DESCRIPTION: The main header file and contains the global variable for debugmode
//
//****************************************************************/

#ifndef MAIN_H
#define MAIN_H

#include <stdio.h>
#include <string.h>
#include "menu.h"
#include "iofunctions.h"

extern int debugmode;

#define DEBUG(fmt, ...) if(debugmode == 1) { printf("DEBUG | "); printf(fmt, ##__VA_ARGS__); printf("\n"); } else (void)0

#endif
