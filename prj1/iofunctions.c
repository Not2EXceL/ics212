/*****************************************************************
//
// NAME: Richmond Steele
//
// HOMEWORK: 5
//
// CLASS: ICS 212
//
// INSTRUCTOR: Ravi Narayan
//
// DATE: February 20, 2016
//
// FILE: iofunctions.c
//
// DESCRIPTION: This file contains the definitions for readfile and writefile
//
//****************************************************************/
#include "iofunctions.h"

/*****************************************************************
//
// Function name: readfile
//
// DESCRIPTION: Reads from the provided filename, the record data into the passed array
//
// Parameters: 2 (struct account**, char[]) : account list, filename
//
// Return values: 1 (int) : number of records read
//
//****************************************************************/
int readfile(struct record** records, char filename[])
{
    FILE* file;
    int count = 0;
    char tempName[25] = { 0 };
    int tempAccountNo = 0;
    char tempAddress[80] = { 0 };
    int tempYearofbirth = 0;
    char buffer[100];
    int read = 0;
    struct record* prevRecord = NULL;
    struct record* curRecord = NULL;

    DEBUG("[readfile] records: %p, filename: %s", &records, filename);

    file = fopen(filename, "r+");
    if(file == NULL)
    {
        printf("File does not exist.\n");
        return -1;
    }

    while(fgets(buffer, sizeof(buffer), file))
    {
        curRecord = (struct record*) malloc(sizeof(struct record));
        sscanf(buffer, "AccountNo: %d\tYearOfBirth: %d\tName: %24[0-9a-zA-Z ]\tAddress: %79[0-9a-zA-Z \t]\n",
               &tempAccountNo, &tempYearofbirth, tempName, tempAddress);
        strncpy(curRecord->name, tempName, 24);
        strncpy(curRecord->address, tempAddress, 79);
        curRecord->accountno = tempAccountNo;
        curRecord->yearofbirth = tempYearofbirth;
        if(records[0] == NULL && prevRecord == NULL)
        {
            records[0] = curRecord;
        } else
        {
            prevRecord->next = curRecord;
        }
        prevRecord = curRecord;
        count++;
        read = 1;
    }
    fclose(file);
    printf("Read %d records from file.\n", count);
    return count;
}


/*****************************************************************
//
// Function name: writefile
//
// DESCRIPTION: Writes to the provided filename, the record data from the passed array
//
// Parameters: 2 (struct record*, char[]) : record array, filename
//
// Return values: 0 :
//
//****************************************************************/
void writefile(struct record* _record, char filename[])
{
    FILE* file;
    int count = 0;

    DEBUG("[writefile] records: %p, filename: %s", &_record, filename);

    file = fopen(filename, "w+");

    if(file == NULL)
    {
        printf("File does not exist.\n");
        return;
    }

    while(_record != NULL)
    {
        fprintf(file, "AccountNo: %d\tYearOfBirth: %d\tName: %s\tAddress: %s\n",
                _record->accountno,
                _record->yearofbirth,
                _record->name,
                _record->address);
        _record = _record->next;
        count++;
    }
    fclose(file);
    printf("Wrote %d records to file.\n", count);
}
