/*****************************************************************
//
// NAME: Richmond Steele
//
// HOMEWORK: 3
//
// CLASS: ICS 212
//
// INSTRUCTOR: Ravi Narayan
//
// DATE: February 06, 2016
//
// FILE: main.c
//
// DESCRIPTION: This file contains the main function
//
//****************************************************************/

#include "main.h"

int debugmode = 0;

/*****************************************************************
//
// Function name: main
//
// DESCRIPTION: A main function that runs test functions and provides them with test data
//
// Parameters: 0 () :
//
// Return values: 0 : exit value, -1 if argument error
//
//****************************************************************/
int main(int argc, char** argv)
{
    struct record* start = NULL;
    switch(argc)
    {
        case 1:
            break;
        case 2:
            DEBUG("1 Arg");
            if(strcmp(argv[1], "debug") == 0)
            {
                debugmode = 1;
                DEBUG("debugmode enabled.");
                break;
            }
        default:
            printf("hw3main: Invalid argument (%d)\n", argc);
            printf("Usage: hw3main [debug]");
            return -1;
    }
    readfile(&start, "records.txt");
    menuUI(&start);
    writefile(start, "records.txt");
    return 0;
}
