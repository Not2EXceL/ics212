/*****************************************************************
//
// NAME: Richmond Steele
//
// HOMEWORK: 3
//
// CLASS: ICS 212
//
// INSTRUCTOR: Ravi Narayan
//
// DATE: February 06, 2016
//
// FILE: record.h
//
// DESCRIPTION: This file contains the record struct
//
//****************************************************************/

#ifndef RECORD_H
#define RECORD_H

struct record
{
    int accountno;
    char name[25];
    char address[80];
    int yearofbirth;
    struct record* next;
};

#endif
