#ifndef ICS212_TCPHEADER_H
#define ICS212_TCPHEADER_H

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdbool.h>

typedef struct
{
    unsigned char sourcePort[2];
    unsigned char destPort[2];
    unsigned char sequenceNo[4];
    unsigned char ackNo[4];
    unsigned char dataOffset_reserved_controlBits[2];
    unsigned char window[2];
    unsigned char checksum[2];
    unsigned char urgentPointer[2];
} TCP_Header;

int headerGetControlSYN(TCP_Header* source);
void headerSetControlACK(TCP_Header* source);
void headerPrint(TCP_Header* source);
void printBits(unsigned char* array, int size, bool newline);
void printBitRange(unsigned char quad, int low, int high, bool newline);
void printBit(unsigned char quad, int bit, bool newline);
TCP_Header* headerClone(TCP_Header* source);
void headerCopy(TCP_Header* dest, TCP_Header* source);
void headerToCharArray(TCP_Header* source, unsigned char* array, int size);
void headerFromCharArray(TCP_Header* source, unsigned char* array, int size);

#endif //ICS212_TCPHEADER_H
