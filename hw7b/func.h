#ifndef ICS212_FUNC_H
#define ICS212_FUNC_H

#include "tcpheader.h"

extern TCP_Header header;
extern TCP_Header response;
extern char* fileName;
extern char* rfileName;

int printheader();

int readfile(char[]);

int writefile(char[]);

#endif //ICS212_FUNC_H
