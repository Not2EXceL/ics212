#include "func.h"

int printheader()
{
    printf("Printing Header.\n");
    headerPrint(&header);
    printf("Printing Response.\n");
    headerPrint(&response);
    return 1;
}

int readfile(char buffer[])
{
    FILE *fp;
    int bytes_read;

    fp = fopen(fileName, "r");
    if (!fp)
    {
        perror("fopen failed");
        return 1;
    }

    bytes_read = fread(buffer, 20, 1, fp);

    fclose(fp);
    return 0;
}

int writefile(char buffer[])
{
    FILE* file;
    int written = 0;
    file = fopen(rfileName, "wb+");
    if (file == NULL)
    {
        return -1;
    }
    written = fwrite(buffer, 1, 20, file);
    return 1;
}