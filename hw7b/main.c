#include "func.h"

TCP_Header header;
TCP_Header response;
char* fileName = "test.bin";
char* rfileName = "test_response.bin";

int main(int argc, char* argv[])
{
    unsigned long sequenceNumber = 0;
    char bufferHeader[1024];
    char bufferResponse[1024];

    readfile(bufferHeader);
    headerFromCharArray(&header, bufferHeader, sizeof(bufferHeader));
    headerCopy(&response, &header);
    for (int i = 0; i < 20; ++i)
    {
        printf("%02x ", bufferHeader[i]);
    }
    printf("\n");

    //update response header with appropriate values
    memcpy(response.sourcePort, header.destPort, 2);
    memcpy(response.destPort, header.sourcePort, 2);
    sequenceNumber = header.sequenceNo[0] | (header.sequenceNo[1] << 8) | (header.sequenceNo[2] << 16) | (header.sequenceNo[3] << 24);
    ++sequenceNumber;
    response.sequenceNo[0] = sequenceNumber & 0xFF;
    response.sequenceNo[1] = (sequenceNumber >> 8) & 0xFF;
    response.sequenceNo[2] = (sequenceNumber >> 16) & 0xFF;
    response.sequenceNo[3] = (sequenceNumber >> 24) & 0xFF;
    memcpy(response.ackNo, header.sequenceNo, 4);
    if (headerGetControlSYN(&header) == 1)
    {
        headerSetControlSYN(&response);
        headerSetControlACK(&response);
    }
    //printheader();
    getch();
}
