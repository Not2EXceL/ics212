#include "tcpheader.h"

int headerGetControlSYN(TCP_Header* source)
{
    return (source->dataOffset_reserved_controlBits[1] >> 6) & 1;
}

void headerSetControlSYN(TCP_Header* source)
{
    source->dataOffset_reserved_controlBits[1] |= (1 << 6);
}

void headerSetControlACK(TCP_Header* source)
{
    source->dataOffset_reserved_controlBits[1] |= (1 << 3);
}

void headerPrint(TCP_Header* source)
{
    printf("========== TCP Header ==========\n");
    printf("Source port: ");
    printBits(source->sourcePort, 2, true);
    printf("Destination port: ");
    printBits(source->destPort, 2, true);
    printf("Sequence number: ");
    printBits(source->sequenceNo, 4, true);
    printf("Acknowledgment number: ");
    printBits(source->ackNo, 4, true);
    printf("Data offset: ");
    printBits(source->dataOffset_reserved_controlBits[0], 0, 4, true);
    printf("Reserved: ");
    printBitRange(source->dataOffset_reserved_controlBits[0], 4, 8, false);
    printBitRange(source->dataOffset_reserved_controlBits[1], 0, 2, true);
    printf("Control bit URG: ");
    printBit(source->dataOffset_reserved_controlBits[1], 2, true);
    printf("Control bit ACK: ");
    printBit(source->dataOffset_reserved_controlBits[1], 3, true);
    printf("Control bit PSH: ");
    printBit(source->dataOffset_reserved_controlBits[1], 4, true);
    printf("Control bit RST: ");
    printBit(source->dataOffset_reserved_controlBits[1], 5, true);
    printf("Control bit SYN: ");
    printBit(source->dataOffset_reserved_controlBits[1], 6, true);
    printf("Control bit FIN: ");
    printBit(source->dataOffset_reserved_controlBits[1], 7, true);
    printf("Window: ");
    printBits(source->window, 2, true);
    printf("Checksum: ");
    printBits(source->checksum, 2, true);
    printf("Urgent pointer: ");
    printBits(source->urgentPointer, 2, true);
    printf("========== TCP Header ==========\n");
    printf("Hex: %02x %02x %02x %02x | %02x %02x %02x %02x | %02x %02x %02x %02x | %02x %02x %02x %02x | %02x %02x %02x %02x\ns",
           source->sourcePort[0], source->sourcePort[1],
           source->destPort[0], source->destPort[1],
           source->sequenceNo[0], source->sequenceNo[1], source->sequenceNo[2], source->sequenceNo[3],
           source->ackNo[0], source->ackNo[1], source->ackNo[2], source->ackNo[3],
           source->dataOffset_reserved_controlBits[0], source->dataOffset_reserved_controlBits[1],
           source->window[0], source->window[1],
           source->checksum[0], source->checksum[1],
           source->urgentPointer[0], source->urgentPointer[1]);
    printf("========== TCP Header ==========\n");
}

void printBits(unsigned char* array, int size, bool newline)
{
    int i = 0;
    int j = 0;
    for (int i = 0; i < size; ++i)
    {
        for (j = 0; j < 4; ++j)
        {
            printf("%d", ((array[i] >> j) & 1));
        }
    }
    if (newline)
    {
        printf("\n");
    }
}

void printBitRange(unsigned char quad, int low, int high, bool newline)
{
    int i = low;
    for (i = low; i < high; ++i)
    {
        printf("%d", ((quad >> i) & 1));
    }
    if (newline)
    {
        printf("\n");
    }
}

void printBit(unsigned char quad, int bit, bool newline)
{
    printf("%d", ((quad >> bit) & 1));
    if (newline)
    {
        printf("\n");
    }
}

TCP_Header* headerClone(TCP_Header* source)
{
    TCP_Header* clone = malloc(sizeof(TCP_Header));
    memcpy(clone->sourcePort, source->sourcePort, 2);
    memcpy(clone->destPort, source->destPort, 2);
    memcpy(clone->sequenceNo, source->sequenceNo, 4);
    memcpy(clone->ackNo, source->ackNo, 4);
    memcpy(clone->dataOffset_reserved_controlBits, source->dataOffset_reserved_controlBits, 2);
    memcpy(clone->window, source->window, 2);
    memcpy(clone->checksum, source->checksum, 2);
    memcpy(clone->urgentPointer, source->urgentPointer, 2);
    return clone;
}

void headerCopy(TCP_Header* dest, TCP_Header* source)
{
    memcpy(dest->sourcePort, source->sourcePort, 2);
    memcpy(dest->destPort, source->destPort, 2);
    memcpy(dest->sequenceNo, source->sequenceNo, 4);
    memcpy(dest->ackNo, source->ackNo, 4);
    memcpy(dest->dataOffset_reserved_controlBits, source->dataOffset_reserved_controlBits, 2);
    memcpy(dest->window, source->window, 2);
    memcpy(dest->checksum, source->checksum, 2);
    memcpy(dest->urgentPointer, source->urgentPointer, 2);
}

void headerToCharArray(TCP_Header* source, unsigned char* array, int size)
{
    if (size < 20)
    {
        printf("[ERROR] Cannot copy data to an array smaller than 20 bytes.\n");
        return;
    }
    memcpy(array, source->sourcePort, 2);
    memcpy(array + 2, source->destPort, 2);
    memcpy(array + 4, source->sequenceNo, 4);
    memcpy(array + 8, source->ackNo, 4);
    memcpy(array + 12, source->dataOffset_reserved_controlBits, 2);
    memcpy(array + 14, source->window, 2);
    memcpy(array + 16, source->checksum, 2);
    memcpy(array + 18, source->urgentPointer, 2);
}

void headerFromCharArray(TCP_Header* source, unsigned char* array, int size)
{
    if (size < 20)
    {
        printf("[ERROR] Cannot copy data to an array smaller than 20 bytes.\n");
        return;
    }
    memcpy(source->sourcePort, array, 2);
    memcpy(source->destPort, array + 2, 2);
    memcpy(source->sequenceNo, array + 4, 4);
    memcpy(source->ackNo, array + 8, 4);
    memcpy(source->dataOffset_reserved_controlBits, array + 12, 2);
    memcpy(source->window, array + 14, 2);
    memcpy(source->checksum, array + 16, 2);
    memcpy(source->urgentPointer, array + 18, 2);
}
